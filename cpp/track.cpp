#include "track.h"
using jsoncons::json;
using jsoncons::json_exception;
using jsoncons::json_deserializer;
using jsoncons::json_reader;
using jsoncons::pretty_print;
using jsoncons::output_format;
using std::string;

#include <boost/math/constants/constants.hpp>

const double pi = boost::math::constants::pi<double>();


static double arcLength(double angle, double radius){
  return (double)2 * pi * radius * (angle / (double)360);
}


CSegment::CSegment(json& segment){

  isSwitch = segment.has_member("switch") ? true : false;




  if(segment.has_member("angle")){
    angle = segment["angle"].as<double>();
    radius = segment["radius"].as<double>();
    

    if(angle < 0){
      type = LEFT;
      angle *= -1;
    }else{
      type = RIGHT;
    }
    
    getLength = [this](double offset) { return arcLength(angle, offset+radius); };
    
    
  }else{
    type = STRAIGHT;
    radius = 0;
    angle = 0;
    length = segment["length"].as<double>();

    getLength = [this](double offset) { return length; };
  }
      

}

CSegment::CSegment(const CSegment& segment) :
  isSwitch(segment.isSwitch),
  type(segment.type),
  radius(segment.radius),
  angle(segment.angle),
  length(segment.length),
  getLength(segment.getLength)
  
{
  

}


void CTrack::print(){

}


CTrack::CTrack(){
}

void CTrack::setup(const jsoncons::json& track){

  segments.clear();

  json pieces = track["pieces"];
  json lanes = track["lanes"];

  numLanes = 0;
  for (size_t i = 0; i < lanes.size(); ++i)
    {
      json& lane = lanes[i];
 
      int index = lane["index"].as<double>();
      double offset = lane["distanceFromCenter"].as<double>();
	
      laneOffsets[index] = offset;
      numLanes++;
    }

  for (size_t i = 0; i < pieces.size(); ++i)
    {
      json& segment = pieces[i];
 

      CSegment seg(segment);

      seg.getLength(2);
      segments.push_back(seg);

    }


}
