#include "dashboard.h"
using jsoncons::json;
using jsoncons::json_exception;
using jsoncons::json_deserializer;
using jsoncons::json_reader;
using jsoncons::pretty_print;
using jsoncons::output_format;
using std::string;

CDashboard::CDashboard(){

}

void CDashboard::setup(CTrack & t, jsoncons::json& cars_data){
  track = &t;
  
  std::cout << "cars: " << pretty_print(cars_data) << std::endl;

  for (size_t i = 0; i < cars_data.size(); ++i){
    json& car_data = cars_data[i];

    json& id = car_data["id"];

    std::string name = id["name"].as<std::string>();

    if(name.compare("AI-I-IO") == 0){
      json& dimensions = car_data["dimensions"];
      
      guideFlagPosition = dimensions["guideFlagPosition"].as<double>();
      length = dimensions["length"].as<double>();
      width = dimensions["width"].as<double>();


      break;
    }
  }

}

void CDashboard::update(const jsoncons::json& data){


}

void CDashboard::updateThrottle(double throttle){


}


double CDashboard::getSpeed(){
  return 0;
}

double CDashboard::getThrottle(){
  return 0;
}
