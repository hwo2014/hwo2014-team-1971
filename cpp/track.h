#ifndef TRACK_H
#define TRACK_H

#include <vector>
#include "protocol.h"
#include <functional>

#define MAX_TRACKS 4

using jsoncons::json;

enum SegmentType {LEFT, RIGHT, STRAIGHT};

class CSegment{
 public:
  bool isSwitch;

  SegmentType type;
  double radius;
  double angle;
  double length;
  
  std::function<double(double)> getLength;
 
  double getRadius() { return radius; }
  double getAngle() { return angle; }
  SegmentType getSegmentType() { return type; }
  bool getIsSwitch() {return isSwitch; }

  CSegment(const CSegment& segment);
  CSegment(json& segment);
};

class CTrack{
 private:
  std::vector<CSegment> segments;
  int laneOffsets[MAX_TRACKS];
  unsigned int numLanes;

 public:
  CTrack();
  void setup(const jsoncons::json& data);
  void print();
};

#endif
