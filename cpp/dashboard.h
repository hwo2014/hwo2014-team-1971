#ifndef DASHBOARD_H
#define DASHBOARD_H

#include "protocol.h"
#include "track.h"


using jsoncons::json;

class CDashboard{

 private:
  CTrack * track;

  double guideFlagPosition;
  double length;
  double width;
  
 public:
  CDashboard();
  void setup(CTrack & t, jsoncons::json& cars_data);

  void update(const jsoncons::json& data);

  void updateThrottle(double throttle);

  double getSpeed();
  double getThrottle();

};


#endif
