#include "game_logic.h"
#include "protocol.h"

#include "track.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "turboAvailable", &game_logic::on_turbo_available }
    }
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;

  json race = data["race"];
  json track_data = race["track"];
  json cars_data = race["cars"];
  
  std::cout << "Setup track" << std::endl;

  track.setup(track_data);
  
  std::cout << "Setup dashboard" << std::endl;

  dashboard.setup(track, cars_data);
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data)
{
  std::cout << "Turbo available" << std::endl;
  
  std::cout << pretty_print(data) << std::endl;

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  dashboard.update(data);

  double throttleVal = throttle.get(dashboard, track);

  dashboard.updateThrottle(throttleVal);
		   
  game_logic::msg_vector vec;

  vec.push_back(make_switch_left());

  vec.push_back(make_throttle(throttleVal));

  return vec;
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}
