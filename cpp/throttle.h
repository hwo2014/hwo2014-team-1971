#ifndef THROTTLE_H
#define THROTTLE_H

#include "dashboard.h"
#include "track.h"


class CThrottle{
 

 public:
  double get(CDashboard & db, CTrack & t);
};

#endif
